Biggest takeaways:
	- We don't need to truly perform the tcp handshake
		+ This is because the gfw only looks for a pattern of syn, ack, data from just our side.
		  The gfw will probably not see the syn-ack from the other side so it doesn't look for it.
		+ This means that we don't need to wait for the syn-ack reply from the server
		+ This also means that we don't need to worry about hosts being live anymore

	- I realized that all of these functions should be command line utils so we need to build in
	  some infrastructure for parsing and such

	- Use the sniff functionality to collect all packets that we see not the sr family of functions
		+ This the means that it doesn't matter whether the server of the gfw get's back to us first
		+ We just need a way of parsing the sniffed packets looking for resets from the proper host

	- sniff function should run in a daemon thread (see python threading docs).
	  This means it dies when the main thread ends, which is perfect since these are all com line tools

gfw_detect:
	- we were really overthinking this one
	- just needs to determine if the path from us to the given IP goes through the gfw
	- If we are given an ip that doesn't need to from us through the gfw then that's fine
	- i.e. we don't need to figure out how to route through china

gfw_tracroute:
	- send the sync, ack, data pattern to every host along the path
	- but as said above, no need to wait for the syn-ack

gfw_minstring:
	- recommended doing a traceroute first to determine distance to gfw? not sure about this
	- no need to find live hosts anymore because we just send syn, ack, data