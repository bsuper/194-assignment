Got packet
IP: 166.111.4.100
Is RST: False
Got packet
IP: 166.111.4.100
Is RST: True
Got packet
IP: 166.111.4.100
Is RST: True
The provided string 'GET /?falun HTTP/1.1
Host: www.google.com

' triggers the great firewall and will be tested for a minimum length
Found a blocked ips file for network address: 116.11.0.0/16
--------------------------------------------
Identifying characters that can be removed
--------------------------------------------

Testing string: *ET /?falun HTTP/1.1
Host: www.google.com


On host: 116.11.17.143
Got packet
IP: 116.11.17.143
Is RST: True

Testing string: G*T /?falun HTTP/1.1
Host: www.google.com


On host: 116.11.19.207
Got packet
IP: 116.11.19.207
Is RST: False
Got packet
IP: 116.11.19.207
Is RST: True
Got packet
IP: 116.11.19.207
Is RST: True
Found unneeded char:  E At index: 1

Testing string: G** /?falun HTTP/1.1
Host: www.google.com


On host: 116.11.19.152
Got packet
IP: 116.11.19.152
Is RST: True

Testing string: G*T*/?falun HTTP/1.1
Host: www.google.com


On host: 116.11.20.132
Got packet
IP: 116.11.20.132
Is RST: False
Got packet
IP: 116.11.20.132
Is RST: True

Testing string: G*T *?falun HTTP/1.1
Host: www.google.com


On host: 116.11.20.183
Got packet
IP: 116.11.20.183
Is RST: False
Got packet
IP: 116.11.20.183
Is RST: True

Testing string: G*T /*falun HTTP/1.1
Host: www.google.com


On host: 116.11.22.38
Got packet
IP: 116.11.22.38
Is RST: True

Testing string: G*T /?*alun HTTP/1.1
Host: www.google.com


On host: 116.11.23.246
Got packet
IP: 116.11.23.246
Is RST: False
Got packet
IP: 116.11.23.246
Is RST: True
Got packet
IP: 116.11.23.246
Is RST: True
Found unneeded char:  f At index: 6

Testing string: G*T /?**lun HTTP/1.1
Host: www.google.com


On host: 116.11.24.1

Testing string: G*T /?*a*un HTTP/1.1
Host: www.google.com


On host: 116.11.32.43
Got packet
IP: 116.11.32.43
Is RST: False
Got packet
IP: 116.11.32.43
Is RST: True
Got packet
IP: 116.11.32.43
Is RST: True
Found unneeded char:  l At index: 8

Testing string: G*T /?*a**n HTTP/1.1
Host: www.google.com


On host: 116.11.32.40
Got packet
IP: 116.11.32.40
Is RST: False
Got packet
IP: 116.11.32.40
Is RST: True
Got packet
IP: 116.11.32.40
Is RST: True
Found unneeded char:  u At index: 9

Testing string: G*T /?*a*** HTTP/1.1
Host: www.google.com


On host: 116.11.32.36
Got packet
IP: 116.11.32.36
Is RST: False
Got packet
IP: 116.11.32.36
Is RST: True

Testing string: G*T /?*a**n*HTTP/1.1
Host: www.google.com


On host: 116.11.32.35
Got packet
IP: 116.11.32.35
Is RST: False
Got packet
IP: 116.11.32.35
Is RST: True

Testing string: G*T /?*a**n *TTP/1.1
Host: www.google.com


On host: 116.11.33.53
Got packet
IP: 116.11.33.53
Is RST: False

Testing string: G*T /?*a**n H*TP/1.1
Host: www.google.com


On host: 116.11.34.1
Got packet
IP: 116.11.34.1
Is RST: True
Got packet
IP: 116.11.34.1
Is RST: True
Found unneeded char:  T At index: 13

Testing string: G*T /?*a**n H**P/1.1
Host: www.google.com


On host: 116.11.40.41
Got packet
IP: 116.11.40.41
Is RST: False
Got packet
IP: 116.11.40.41
Is RST: True

Testing string: G*T /?*a**n H*T*/1.1
Host: www.google.com


On host: 116.11.40.8
Got packet
IP: 116.11.40.8
Is RST: True

Testing string: G*T /?*a**n H*TP*1.1
Host: www.google.com


On host: 116.11.40.188

Testing string: G*T /?*a**n H*TP/*.1
Host: www.google.com


On host: 116.11.40.1
Got packet
IP: 116.11.40.1
Is RST: True

Testing string: G*T /?*a**n H*TP/1*1
Host: www.google.com


On host: 116.11.40.183

Testing string: G*T /?*a**n H*TP/1.*
Host: www.google.com


On host: 116.11.40.96
Got packet
IP: 116.11.40.96
Is RST: True

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.google.com


On host: 116.11.40.79
Got packet
IP: 116.11.40.79
Is RST: False
Got packet
IP: 116.11.40.79
Is RST: True
Got packet
IP: 116.11.40.79
Is RST: True
Found unneeded char:  At index: 20

Testing string: G*T /?*a**n H*TP/1.1**Host: www.google.com


On host: 116.11.40.78

Testing string: G*T /?*a**n H*TP/1.1*
*ost: www.google.com


On host: 116.11.40.107

Testing string: G*T /?*a**n H*TP/1.1*
H*st: www.google.com


On host: 116.11.40.166
Got packet
IP: 116.11.40.166
Is RST: False
Got packet
IP: 116.11.40.166
Is RST: True

Testing string: G*T /?*a**n H*TP/1.1*
Ho*t: www.google.com


On host: 116.11.40.102

Testing string: G*T /?*a**n H*TP/1.1*
Hos*: www.google.com


On host: 116.11.40.129

Testing string: G*T /?*a**n H*TP/1.1*
Host* www.google.com


On host: 116.11.40.73

Testing string: G*T /?*a**n H*TP/1.1*
Host:*www.google.com


On host: 116.11.40.72

Testing string: G*T /?*a**n H*TP/1.1*
Host: *ww.google.com


On host: 116.11.40.247

Testing string: G*T /?*a**n H*TP/1.1*
Host: w*w.google.com


On host: 116.11.40.35

Testing string: G*T /?*a**n H*TP/1.1*
Host: ww*.google.com


On host: 116.11.40.50
Got packet
IP: 116.11.40.50
Is RST: True

Testing string: G*T /?*a**n H*TP/1.1*
Host: www*google.com


On host: 116.11.40.30

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.*oogle.com


On host: 116.11.40.19
Got packet
IP: 116.11.40.19
Is RST: True

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.g*ogle.com


On host: 116.11.40.13
Got packet
IP: 116.11.40.13
Is RST: False
Got packet
IP: 116.11.40.13
Is RST: True

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.go*gle.com


On host: 116.11.40.190
Got packet
IP: 116.11.40.190
Is RST: True

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.goo*le.com


On host: 116.11.40.65

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.goog*e.com


On host: 116.11.40.195

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.googl*.com


On host: 116.11.40.158

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.google*com


On host: 116.11.40.117

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.google.*om


On host: 116.11.40.152

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.google.c*m


On host: 116.11.40.63

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.google.co*


On host: 116.11.40.254

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.google.com*


On host: 116.11.40.173

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.google.com*

On host: 116.11.40.134
Got packet
IP: 116.11.40.134
Is RST: False
Got packet
IP: 116.11.40.134
Is RST: True

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.google.com
*

On host: 116.11.40.136

Testing string: G*T /?*a**n H*TP/1.1*
Host: www.google.com
* 
On host: 116.11.40.88
Got packet
IP: 116.11.40.88
Is RST: False
Got packet
IP: 116.11.40.88
Is RST: True
Got packet
IP: 116.11.40.88
Is RST: True
Found unneeded char:  
At index: 45
unneeded_chars [1, 6, 8, 9, 13, 20, 45]
--------------------------------------------
Removing characters to find a minimum string
--------------------------------------------

Testing string: GT /?falun HTTP/1.1
Host: www.google.com


Testing string: GET /?alun HTTP/1.1
Host: www.google.com


Testing string: GET /?faun HTTP/1.1
Host: www.google.com


Got packet
IP: 116.11.41.170
Is RST: False
Got packet
IP: 116.11.41.170
Is RST: True
Got packet
IP: 116.11.41.170
Is RST: True
Success! Updating string to: GET /?faun HTTP/1.1
Host: www.google.com



Testing string: GET /?fau HTTP/1.1
Host: www.google.com


Got packet
IP: 116.11.41.119
Is RST: False
Got packet
IP: 116.11.41.119
Is RST: True
Got packet
IP: 116.11.41.119
Is RST: True
Success! Updating string to: GET /?fau HTTP/1.1
Host: www.google.com



Testing string: GET /?fau HTT/1.1
Host: www.google.com


Testing string: GET /?fau HTTP/1.1
ost: www.google.com


Got packet
IP: 116.11.41.22
Is RST: True
Testing string: GET /?fau HTTP/1.1
Host: www.google.com


Got packet
IP: 116.11.41.24
Is RST: False
Got packet
IP: 116.11.41.24
Is RST: True

--------------------------------------------

Sucessfully Found the min string to be: GET /?fau HTTP/1.1
Host: www.google.com


Original String was: GET /?falun HTTP/1.1
Host: www.google.com


Network Address was: 116.11.0.0/16
--------------------------------------------
