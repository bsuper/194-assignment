import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
from netaddr import IPSet

TIMEOUT = 5

RST = 0x04

def make_dns_query(qname="google.com", dns_server="211.151.242.200"):
    answer = sr1(IP(dst=dns_server)/UDP(dport=53)/DNS(rd=1,qd=DNSQR(qname=qname)),verbose=0)
    return answer[DNS]

def make_http_query(dst, query="falun", host="www.google.com", ttl=99):
    syn = IP(dst=dst) / TCP(dport=80, flags='S')
    syn_ack = sr1(syn, verbose=0)
    if TCP in syn_ack:
        get_str = "GET /?{0} HTTP/1.1\r\nHost: {1}\r\n\r\n".format(query, host)
        req = IP(dst=dst, ttl=ttl) / TCP(dport=80, sport=syn_ack[TCP].dport,
                 seq=syn_ack[TCP].ack, ack=syn_ack[TCP].seq + 1, flags='A') / get_str
        answers, unanswered = sr(req, verbose=0)
        return answers
    if ICMP in syn_ack:
        return syn_ack

def gfw_detect(ip_address="202.116.64.8"):
    # we make a dns query for the ip_address to tsinghua's ns
    # rcode == 5 means that the DNS request was refused
    return make_dns_query(qname=ip_address, dns_server="dns2.edu.cn").rcode == 5L

def gfw_traceroute(ip_address="202.116.64.8"):
    for i in range(1, 99):
        rsps = make_http_query(ip_address, ttl=i, query="falun", host="www.google.com")
        for rsp in rsps:
            print "#TTL: ", i, "SRC: ", rsp[1].src, "# RSPS: ", len(rsps), "SUMMARY: ", rsp[1].summary()
            if TCP in rsp[1] and rsp[1][TCP].flags & RST > 0:
                return i
    return -1

def gfw_minstring(string, network_address):

	addresses = iter(IPSet([network_address]))
	responses = []
	for char_index in range(len(string)):
		try:
			mod_string = string[:char_index] + 'X' + string[char_index+1:]

			print "mod string", mod_string
			response = None
			while (response == None):
				address = str(addresses.next())
				packet = build_tcp_packet(address, mod_string)
				print "next address", address
				print "packet", packet
				response = sr1(packet, timeout=TIMEOUT, verbose=0)
				print response

			responses.append(response)
		except StopIteration:
			address_range = iter(IPSet(network_address))

	for resp in responses:
		print resp

def build_tcp_packet(ip_address, dst_port, string):
	return IP(dst=ip_address)/TCP(dport=dst_port)/string


def gfw_loadmap():
    pass

# A string that should trigger the firewall
TEST_STRING = "GET / HTTP/1.1\r\nHost: www.facebook.com\r\n\r\n"

# A chinese address block. Not all hosts are alive in this block
TEST_ADDRESS_BLOCK =  "116.11.0.0/16"

# IP of Tsinghua U
TEST_ADDRESS = "166.111.4.100"

def run_minstring_test():
	gfw_minstring(TEST_STRING, TEST_ADDRESS_BLOCK)

# This should receive a tcp reset in response to it
def send_test_packet():
	ans,unans = sr1(build_tcp_packet(TEST_ADDRESS, 80, TEST_STRING), timeout=TIMEOUT, verbose=0)
	print ans
	print unans

# run_minstring_test()
# send_test_packet()
yt = make_dns_query(qname="youtube.com")
gl = make_dns_query(qname="google.com")
fb = make_dns_query(qname="facebook.com")
al = make_dns_query(qname="aliexpress.com")
