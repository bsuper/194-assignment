#!/usr/bin/env python
import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
import threading
import time
import sys
from Sniffer import *
import socket
import random

TIMEOUT = 3

# A string that reliably triggers the gfw
TRIGGER_STRING = "GET /?falun HTTP/1.1\r\nHost: www.google.com\r\n\r\n"
# A string that doesn't trigger the gfw
SAFE_STRING = "GET / HTTP/1.1\r\nHost: www.wechat.com\r\n\r\n"

# A chinese address block
TEST_ADDRESS_BLOCK =  "116.11.0.0/16"

# IP of Tsinghua U
TEST_ADDRESS = "166.111.4.100"

# Number of resets for us to determine firewall is blocking
GFW_RST_THRESH = 1

RST = 0x04

def is_tcp_rst(p):
    """Returns true iff this packet is a tcp rst"""
    return p.haslayer(TCP) and (p['TCP'].flags & RST != 0)

def color_text(text, color="red"):
    col_str = "{}"
    if color == "red":
        col_str = "\033[91m {}\033[00m"
    elif color == "green":
        col_str = "\033[92m {}\033[00m"
    elif color == "purple":
        col_str = "\033[95m {}\033[00m"
    
    return col_str.format(text)

def count_rsts(pkts):
    """Count the number of rsts in pkts"""
    total = 0
    for pkt in pkts:
        if (is_tcp_rst(pkt)):
            total += 1
    return total

def print_detect_results(host, blocked):
    """Prints whether the path to the given hosts is blocked"""
    option0 = "Failed:"
    option1 = "does not go"
    if (blocked):
        option0 = "Success:"
        option1 = "goes"
    print "%s Path to %s %s through the Great Firewall of China" % (option0, host, option1)

def build_pkt(dst, data_string="", sport=20, dport=80, seq=1, ack=1, flags='A', ttl=99):
    """Build a TCP/IP packet. Allows for a lot of configuration"""
    return IP(dst=dst, ttl=ttl) / TCP(sport=sport, dport=dport, seq=seq, ack=ack, flags=flags) / data_string

def send_http_query(dst, query="falun", host="www.google.com", data_string=None, ttl=99):
    """
    Sends the syn, ack, data pattern recognized by the gfw

    Keyword Arguments:
    dst         -- an ip address. may use an address of the form 1.2.3.0-254 to send multiple packets
    data_string -- the string to send as http data
    """
    syn = build_pkt(dst, flags='S', seq=0, ack=0, ttl=ttl)
    ack = build_pkt(dst, ttl=ttl)

    if not data_string:
        data_string = "GET /?{0} HTTP/1.1\r\nHost: {1}\r\n\r\n".format(query, host)

    data = build_pkt(dst, data_string, ttl=ttl)

    # I found that without putting a little bit of spacing the gfw would miss the flow
    wait_secs = .2
    send(syn, verbose=0)
    time.sleep(wait_secs)
    send(ack, verbose=0)
    time.sleep(wait_secs)
    send(data, verbose=0)

class OutOfIPException(Exception):
    """Signal that the address block has been exhausted of addresses"""
    def __init__(self):
        pass

class GfwHostFinder:
    """
    Used to incrementally return a list of ips that are being sensored by the GFW from the given ip address block.
    liverange is the key function to look at in this class

    Keyword Arguments:
    network_address -- a string representing a /16 block of ip addresses
    write_to_file   -- write blocked ips to a file (default False)
    """

    def __init__(self, network_address, write_to_file=False, file_name="blocked_ips.txt"):
        self.top_addresses = self.get_net_address(network_address)
        self.write_to_file = write_to_file
        self.index = 0
        self.out_file_name = file_name
        self.blocked_ip_count = 0
        self.my_ip = socket.gethostbyname(socket.gethostname())
        self.filter = "src net %s" % (self.top_addresses + ".0.0/16")
        self.rst_counts = {}

        print "Your ip address is: ", self.my_ip

        # Open a new file/clear the file before writing to it
        if self.write_to_file:
            self.out_file = open(self.out_file_name, 'w')

    def get_net_address(self, address):
        """
        For an address like 1.2.3.4
        Returns 1.2
        """
        return ".".join(address.split(".")[:2])

    def rst_counter(self, pkt):
        """
        Increments the rst count for a given src ip address
        Passed to the sniffer and called on each received packet
        """
        if is_tcp_rst(pkt):
            src = pkt['IP'].src
            print color_text("RST from:", color="red"), color_text(src, color="purple")
            if src in self.rst_counts:
                self.rst_counts[src] += 1
            else:
                self.rst_counts[src] = 1

    def generate_rst_counts(self, hosts, string):
        """
        Returns a dictionary mapping host src to the number of rsts returned from that host's responses
        Note: Hosts should be a string formatted as 1.1.1.0-254 == 1.1.1.0/24
        """
        # Send packets to all hosts and collect responses
        self.rst_counts = {}
        sniffer = HostSniffer(filter_string=self.filter, timeout=TIMEOUT, pkt_func=self.rst_counter)
        send_http_query(hosts, data_string=string)
        time.sleep(TIMEOUT + 0.1)

    def liverange(self):
        """
        Process the next 255 ips from self.network_address, returning those that are blocked, but
        do not return rsts for a valid query
        """
        if (self.index > 255):
            if self.write_to_file:
                self.out_file.close()
            raise OutOfIPException()

        hosts = "%s.%d.0-254" % (self.top_addresses, self.index)

        display_str = "Finding hosts from range: %s" % (hosts)
        border = "-" * len(display_str)
        print "%s\n%s\n%s" % (border, display_str, border)

        # See if hosts respond with rsts to TRIGGER_STRING
        self.generate_rst_counts(hosts, TRIGGER_STRING)
        blocked_ips = [src for src in self.rst_counts.keys() if self.rst_counts[src] >= GFW_RST_THRESH]

        # prepare for next call to this function
        num_blocked = len(blocked_ips)
        self.index += 1
        self.blocked_ip_count += num_blocked

        # Write to file
        if self.write_to_file and num_blocked > 0:
            self.out_file.write("\n".join(blocked_ips) + "\n")

        if num_blocked > 0:
            print color_text("Blocked ips:\n" + " | ".join(blocked_ips), color="green")
        return blocked_ips

##################
#                #
# MAIN UTILITIES #
#                #
##################

def gfw_detect(host, k=5, data_string=None, ttl=99, print_res=True, print_ttl=True):
    for i in range(k):
        # print "Checking if {0} is on path to the great firewall - Attempt {1}".format(host, i)
        blocked, icmp_host = _gfw_detect(host, data_string=data_string, ttl=ttl, print_res=print_res, print_ttl=print_ttl)
        if blocked:
            break

        if icmp_host:
            print "ICMP TTL Exceeded From: %15s" % (icmp_host),
            break

    if (print_res):
        print_detect_results(host, blocked)

    return blocked

def _gfw_detect(host, data_string=None, ttl=99, print_res=True, print_ttl=True):
    """
    Using either TRIGGER_STRING or SAFE_STRING, 
    returns whether the path to host IS BLOCKED
    by detecting a stream of reset packets using GFW_RST_THRESH

    Keyword arguments:
    host         -- the target ip address
    use_safe     -- use SAFE_STRING instead of TRIGGER_STRING (default False)
    ttl          -- packet ttl (default 99)
    print_res    -- print results (default True)
    """
    sniffer = HostSniffer(host=host, timeout=TIMEOUT)
    if data_string:
        send_http_query(host, data_string=data_string, ttl=ttl)
    else:
        send_http_query(host, data_string=TRIGGER_STRING, ttl=ttl)
    time.sleep(TIMEOUT)
    host_responses = sniffer.get_packets()

    icmp_host = None
    if print_ttl and len(host_responses):
        if "time-exceeded" in host_responses[0].summary() or "ttl-zero-during-transit" in host_responses[0].summary():
            icmp_host = host_responses[0]['IP'].src


    blocked = count_rsts(host_responses) >= GFW_RST_THRESH

    return blocked, icmp_host

def gfw_traceroute(ip_address, max_hops=50):

    if not gfw_detect(ip_address):
        print "Traceroute will not be conducted.\nExiting..."
        return

    for i in range(1, max_hops):
        print "%2d.)" % (i),
        blocked = gfw_detect(ip_address, ttl=i, print_res=False)

        if blocked:
            print color_text("Firewall Detected!\nExiting....", color="red")
            break

        print color_text("Not Detected", color="purple")

def gfw_liverange(network_address, num_hosts=None):
    """
    Scan the network_address range until we have found at least the requested number of hosts
    or the address range is depleted. Writing blocked hosts to a file
    """
    # Can't have a '/' in our filename
    network_address_str = network_address.split("/")[0]
    file_name = "blocked_ips-%s.txt" % (network_address_str)
    hostFinder = GfwHostFinder(network_address, write_to_file=True, file_name=file_name)
    start_time = time.time()
    try:
        while(True):
            hostFinder.liverange()
            if num_hosts and (hostFinder.blocked_ip_count >= num_hosts):
                break
    except OutOfIPException:
        pass

    print "Found %d blocked ips for network address: %s" % (hostFinder.blocked_ip_count, network_address)
    print "Stored ip addresses into: %s" % (file_name)
    print "Time Elapsed: ", time.time() - start_time, "seconds"

def gfw_minstring(string, network_address):

    # Can't have a '/' in our filename
    network_address_str = network_address.split("/")[0]
    file_name = "blocked_ips-%s.txt" % (network_address_str)

    # Check if this string triggers the firewall. Stop if it does not
    if (not gfw_detect(TEST_ADDRESS, print_res=False)):
        print "The provided string '%s' does not trigger the great firewall" % (string)
        return

    print "The provided string '%s' triggers the great firewall and will be tested for a minimum length" % (string)

    # Then see if there is an existing blocked ips file. Stop if one does not exist
    try:
        with open(file_name, "r") as fp:
            print "Found a blocked ips file for network address: %s" % (network_address)
            hosts = [line.strip() for line in fp]
    except IOError as e:
        print ("A blocked ips file for network address: %s does not yet exist" % (network_address) +
                "Try running gfw liverange first")
        return

    unneeded_chars = []
    random.shuffle(hosts)
    hosts = iter(hosts)
    try:
        print "--------------------------------------------"
        print "Identifying characters that can be removed"
        print "--------------------------------------------"
        star_string = string
        for char_index in range(len(string)):

            mod_string = star_string[:char_index] + '*' + star_string[char_index+1:]
            next_host = hosts.next()

            print "\nTesting string:", mod_string, "\nOn host:", next_host
            blocked = gfw_detect(next_host, data_string=mod_string, print_res=False)

            if blocked:
                print "Found unneeded char: ", string[char_index], "At index:", char_index
                unneeded_chars.append(char_index)
                star_string = mod_string

        # Then remove as many characters as we can
        print "--------------------------------------------"
        print "Removing characters to find a minimum string"
        print "--------------------------------------------\n"
        min_string = string
        for char_index in unneeded_chars:
            mod_string = min_string[:char_index] + min_string[char_index+1:]
            next_host = hosts.next()

            print "Testing string:", mod_string
            blocked = gfw_detect(next_host, data_string=mod_string, print_res=False)

            if blocked:
                print "Success! Updating string to: %s\n" % (mod_string)
                min_string = mod_string

        print "\n--------------------------------------------"
        print "\nSucessfully Found the min string to be:", min_string
        print "Original String was:", string
        print "Network Address was:", network_address
        print "--------------------------------------------"


    except StopIteration:
        print "Could not find an answer for address block:", network_address, "ran out of hosts."
        return

def usage():
    print (
        "\nSupported utilities:\n"
        "detect <IP_ADDRESS>\n"
        "traceroute <IP_ADDRESS>\n"
        "liverange <IP_ADDRESS/16> [<MIN_NUMBER_OF_HOSTS_WANTED>]\n"
        "minstring <STRING> <IP_ADDRESS/16>\n"
    )

features = {
    "detect": {"func": gfw_detect, "num_args": 1},
    "traceroute": {"func": gfw_traceroute, "num_args": 1},
    "liverange": {"func": gfw_liverange, "num_args": 1},
    "minstring": {"func": gfw_minstring, "num_args": 2}
}

def valid_args(args):
    if len(args) < 1:
        print "No function specified"
        return False

    if not args[0] in features:
        print "Function not found:", args[0]
        return False

    num_args = features[args[0]]["num_args"]
    if not num_args == len(args[1:]):
        print "Incorrect number of arguments to %s. Expected: %d, but was: %d" % (args[0], num_args, len(args[1:]))
        return False

    return True

if __name__ == "__main__":
    args = sys.argv[1:]

    if (valid_args(args)):
        features[args[0]]["func"](*args[1:])
    else:
        usage()
