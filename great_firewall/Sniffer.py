# This file houses the sniffer class(es) that are used to record and filter packets
# for our command line utilities
#
# All sniffers run their packet sniffing on a seperate thread

import threading
from scapy.all import *

def is_tcp_rst(p):
    """Returns true iff this packet is a tcp rst"""
    return p.haslayer(TCP) and (p['TCP'].flags & 0x04 != 0)

# Records all packets from host
class HostSniffer:

    def __init__(self, host=None, filter_string=None, pkt_func=None, timeout=None):
        self.host = host
        self.packets = []
        self.timeout = timeout
        self.pkt_func = pkt_func

        if filter_string:
            self.filter = filter_string
        else:
            self.filter = "src %s or icmp" % self.host

        self.thread = threading.Thread(target=self.start_sniffing)
        self.thread.daemon = True
        self.thread.start()

    def start_sniffing(self):
        action_func = self.sniffer
        if self.pkt_func:
            action_func = self.pkt_func

        sniff(prn=action_func,
            filter=self.filter,
            store=0,
            timeout=self.timeout
        )

    def sniffer(self, pkt):
        self.packets.append(pkt)

    def get_packets(self):
        return self.packets

    def clear_packets(self):
        self.packets = []
