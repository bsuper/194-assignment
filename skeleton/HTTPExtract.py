#!/usr/bin/env python

import sys
import gzip
import StringIO

def http_extract(request, reply):
    req = open(request, 'r').read()
    rep = open(reply, 'r').read()

    files = []
    types = []
    payloads = []
    headers = req.split('\r\n\r\n')
    for item in headers:
        if item.split('\r\n')[0].split() != []:
            filename = item.split('\r\n')[0].split()[1]
            files.append(filename)
    for x in range(len(files)):
        rep_headers = rep.split('\r\n\r\n', 1)[0]
        all_headers = rep_headers.split('\r\n')
        mime_type = None
        content_length = False
        chunked = False
        encoding = None
        for h in all_headers:
            cmd = h.split(':')[0].lower()
            if cmd == 'content-type':
                mime_type = h.split(':',1)[1].strip()
            elif cmd == 'content-length':
                content_length = int(h.split(':')[1].strip())
            elif cmd == 'content-encoding':
                encoding = h.split(':')[1].strip().lower()
            elif cmd == 'transfer-encoding':
                chunked = h.split(':')[1].strip().lower() == 'chunked'
        types.append(mime_type)
        payload = None
        if chunked:
            payload = ""
            rep = rep[len(rep_headers) + 4:]
            len_str = rep.split('\r\n')[0]
            len_val = int(len_str, 16)
            while len_val != 0:
                payload += rep[len(len_str) + 2:
                                   len(len_str) + 2 + len_val]
                rep = rep[len(len_str) + 2 + len_val + 1:]
                len_str = rep.split('\r\n')[0]
                len_val = int(len_str, 16)
        elif content_length != None:
            payload = rep[len(rep_headers) + 4: 
                          len(rep_headers) + 4 + content_length]
            if (x < len(files) - 1):
                rep = rep[len(rep_headers) + 4 + content_length:]
        else:
            payload = rep[len(rep_headers) + 4:]

        if encoding == 'gzip':
            payload = gzip.GzipFile(fileobj=StringIO.StringIO(payload)).read()

        payloads.append(payload)



    return (files, types, payloads)

if __name__ == '__main__':
    res = http_extract(sys.argv[1], sys.argv[2])
    print res[0]
    print res[1]
    for file in res[2]:
        print file


