# Based on the old terminate-connection.bro


module InjectConnection;

export {

	# Whether we're allowed (and/or are capable) to terminate connections
	# using "rst".
	const activate_injector = F &redef;

	# where is 'rst' located
	const binary_location = 
	      "/usr/local/bro/bin/rst" &redef;

	# Inject the payload into the given connection
	global inject_connection: function(c: connection,
	       to_orig:bool,
	       payload:string
	       );

       # Inject a 302 redirect
       global inject_302: function(c: connection,
	       to_orig: bool,
	       url: string,
	       with_rst: bool,
	       with_fin: bool);
}


function inject_302(c: connection,
	to_orig: bool,
	url: string,
	with_rst: bool,
	with_fin: bool){
       local payload = "\"HTTP/1.1 302 Moved Temporarily\\nLocation: %s\\nServer: Satan-HTTP-Server\\nContent-Type: text/html; charset=UTF-8\\nCache-Control: no-cache, no-store, max-age=0, must-revalidate\\nContent-Length: 0\\nConnection: close\\n\\n\"";
       inject_connection(c, to_orig,
                         fmt(payload, url));
       if(with_rst){
	       local offset = |fmt(payload, url)|;
	       print offset;
	       }


}

function inject_connection(c: connection,
	 		   to_orig: bool,
			   payload: string){
	local id = c$id;
	if(activate_injector){
	    local term_cmd = fmt("%s %s -I %s -r 4 %s %d %d %s %d %d",
	       binary_location,
	       to_orig ? "-R" : "",
	       # shell escape breaks \r\n\r\n
	       # str_shell_escape(payload),
	       payload, 
	       id$orig_h, id$orig_p, get_orig_seq(id),
	       id$resp_h, id$resp_p, get_resp_seq(id));	       
            print payload;
	    print term_cmd;
            system(term_cmd);
	}
}			  



