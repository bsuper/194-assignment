import NWDNS

import thread
import socket
import sys
import random

import time

import Queue

import traceback

class MessageTracker():
    def __init__(self, message, address, function):
        self.message = message
        self.address = address
        self.function = function
        self.replied = False
        self.timestamp = time.time()


# A class which does fast DNS queries.

# The big item is that receiveLoop is run in a SEPARATE thread,
# thus it shouldn't have a problem processing it, the results are
# then placed in the messageQueue.

# It is then up to the user of the system to call poll() to pull the
# items off the queue for processing.

# To add a query, do send(msg, fn).  When the reply is received,
# fn(msg, response) is called (fn can of course be an object method
# too).  If no reply is received (timeout happens), fn is called with
# (msg, None)

class FastDNSQuerier():
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        # Pointless binding, actually, so removed
        # self.s.bind(('', 5314))

        # A queue for received messages that gets pulled
        # off by poll().
        self.messageQueue = Queue.Queue(0)
        
        # A queue for messages to check for timeouts
        self.timeoutQueue = Queue.Queue(0)

        self.timeoutHead = None

        # Default timeout of 4.0 seconds
        self.timeout = 4.0

        # Default timeout for polling
        self.pollTimeout = .01

        # A dictionary indexed by transaction ID.  Within each
        # transaction ID is list of MessageTrackers
        self.pendingReplies = {}
        thread.start_new_thread(self.receiveLoop, ())

        self.debug = False

    def receiveLoop(self):
        sys.stdout.flush()
        while True:
            try:
                data, address = self.s.recvfrom(8192)
                self.messageQueue.put((data, address))
            except KeyboardInterrupt, info:
                print "Caught control-C, exiting"
                self.s.close()
                sys.exit()
            except:
                sys.stderr.write("Bad received packet.")
                traceback.print_exc()
                sys.stderr.write("Not going to reraise!")
                sys.stdout.flush()


    # Sends a message.  If a response is received,
    # The function is called with (question, response),
    # otherwise it is called with (question, None)

    # Please note that send() and poll() need to be called from within
    # the same thread, as there is no locking of the pendingReplies
    # structure

    def send(self, message, address, function):
        if self.debug:
            print "To address: ", address
            print "Sending query %s (%s) id: %x" % \
                (message.question[0].qname, 
                 NWDNS.rtype_names[message.question[0].qtype],
                 message.header.id)
        message.serverIP = address
        if message.header.id not in self.pendingReplies:
            self.pendingReplies[message.header.id] = set()
        tracker = MessageTracker(message, address, function)
        self.pendingReplies[message.header.id].add(tracker)
        self.timeoutQueue.put(tracker)
        self.s.sendto(message.pack(), (address, 53))

    # Poll calls the receive'd objects when appropriate
    # and keeps track of timeouts.

    def checkTimeout(self):
        if(self.timeoutHead == None):
            try:
                self.timeoutHead = self.timeoutQueue.get(False)
            except Queue.Empty, info:
                return False
        if(self.timeoutHead.replied):
            self.timeoutHead = None
            return True
        curtime = time.time()
        if(curtime - self.timeoutHead.timestamp > self.timeout):
            self.pendingReplies[self.timeoutHead.message.header.id].remove(
                self.timeoutHead)
            try:
                self.timeoutHead.function(self.timeoutHead.message, None)
            except SystemExit:
                sys.exit()
            except:
                sys.stderr.write("Caught exception in delegated function\n")
                traceback.print_exc()
            self.timeoutHead = None
            return True
        return False

    def poll(self):
        datum = ""
        while self.checkTimeout():
            ""
        try:
            # messageQueue.get will raise a 
            # Queue.Empty exception so this will go until
            # there are no packets waiting in the queue.
            while True:
                datum = self.messageQueue.get(True, self.pollTimeout)
                try:
                    message = NWDNS.DNSMessage(datum[0])
                except:
                    sys.stderr.write("Bad received packet/failed to parse.")
                    sys.stderr.write("Received packet from IP %s" % 
                                     datum[1][0])
                    traceback.print_exc()
                    return

                message.serverIP = datum[1][0]
                if self.debug:
                    print "Received:", message.shortRepr()
                    sys.stdout.flush()
                if message.header.id in self.pendingReplies:
                    element = None
                    # RCODE_REFUSED and some others only match on
                    # server IP and TXID, with a 0 length question
                    # in the reply
                    for item in self.pendingReplies[message.header.id]:
                        if(item.address == datum[1][0] and 
                           ((len(message.question) > 0 and
                             item.message.question[0].qname.lower() ==
                             message.question[0].qname.lower())
                            or len(message.question) == 0)):
                                element = item
                    if(element != None):
                        self.pendingReplies[
                            message.header.id].remove(element)
                        element.replied = True
                        try:
                            element.function(element.message, message)
                        except SystemExit:
                            sys.exit()
                        except:
                            sys.stderr.write("Caught exception in delegated function\n")
                            traceback.print_exc()
                    else:
                        if self.debug:
                            print "Unsolicited message %s but ID matches" % message.shortRepr()
                else:
                    if self.debug:
                        print "Unsolicited message %s no ID matches" % message.shortRepr()

        except Queue.Empty, info:
            return self
        except KeyboardInterrupt, info:
            print "Got Ctrl-c, exiting"
            self.s.close()
            sys.exit()


        

RootServers = ['198.41.0.4',
               '192.228.79.201', 
               '192.33.4.12',
               '128.8.10.90',
               '192.203.230.10',
               '192.5.5.241',
               '192.112.36.4',
               '128.63.2.53',
               '192.36.148.17',
               '192.58.128.30',
               '193.0.14.129',
               '199.7.83.42',
               '202.12.27.33']


# A class that does a recursive lookup with no internal cache.  The
# function done() is called with a tuple of (qname, qtype) as the
# first argument and thde final DNS answer as the second argument, or
# None if there is a failure (either a timeout or a lack of sufficient
# glue information)

# ToDo:  Add retry functionality
# ToDo:  Add limit on amount of recursion on the NS RRSet

class RecursiveLookup():
    def __init__(self, querier, name, done, qtype = NWDNS.RTYPE_A, limit = 0):
        self.querier = querier
        self.name = name
        self.done = done
        self.qtype = qtype
        self.question = (name, qtype)
        self.limit = limit
        if(limit > 10):
            done(self.question, None)
            return
        msg = NWDNS.DNSMessage()
        msg.question[0].qname = name
        msg.question[0].qtype = qtype
        self.querier.send(msg, 
                          RootServers[random.randint(0,len(RootServers) - 1)],
                          self.iterate)
        self.retryCount = 0
        self.servs = []

    def cycle(self, msg, response):
        if (response == None or response.header.rcode != NWDNS.RCODE_OK):
            try:
                [ns, addr] = self.servs.pop(0)
                if (addr):
                    msg = NWDNS.DNSMessage().cloneFrom(msg)
                    self.querier.send(msg,
                                      addr,
                                      self.cycle)
                else:
                    RecursiveLookup(self.querier, ns, 
                                    self.resume, NWDNS.RTYPE_A)
            except IndexError:
                self.iterate(msg, response)

        else:
            self.servs = []
            self.iterate(msg, response)

    def resume(self, question, resume):
        msg = NWDNS.DNSMessage()
        msg.question[0].qname = self.name
        msg.question[0].qtype = self.qtype

        if (resume != None):
            servs = filter(lambda a: 
                a.rtype == NWDNS.RTYPE_A,
                resume.answer)
        
            if (servs != []):
                self.servs = map(lambda a: [a.name.lower(), a.rdata], servs) + self.servs
                resume = None
            else:
                resume.header.rcode = NWDNS.RCODE_SERVFAIL
        self.cycle(msg, resume)

    """
    def resume(self, question, resume):
        msg = NWDNS.DNSMessage()
        msg.question[0].qname = self.name
        msg.question[0].qtype = self.qtype
        if(resume == None):
            if(self.done != None):
                self.done(self.question, None)
                return
        for item in resume.answer:
            if(item.rtype == NWDNS.RTYPE_A):
                self.querier.send(msg,
                                  item.rdata,
                                  self.iterate)
                return

        # Unable to analyze this because no A record found
        # for the NS
        if(self.done != None):
            self.done(self.question, None)
    """

    def iterate(self, msg, response):
        if(response == None):
            self.done(self.question, None)
        elif(response.header.rcode != NWDNS.RCODE_OK):
            self.done(self.question, response)
        else:
            if(response.header.ancount > 0):
                if(self.done != None):
                    self.done(self.question, response)
            elif(response.header.nscount > 0):
                for auth in response.authority:
                    if(auth.rtype == NWDNS.RTYPE_SOA):
                        #raise NotImplementedError("Not handling SOA")
                        response.header.rcode = NWDNS.RCODE_SERVFAIL
                        if(self.done != None):
                            self.done(self.question, response)
                        return
                    else:
                        servs = filter(lambda a: a.name.lower() == auth.rdata.lower() and
                                a.rtype == NWDNS.RTYPE_A,
                                response.additional)
                        if (servs != []):
                            self.servs += map(lambda a: [a.name.lower(), a.rdata], servs)
                        else:
                            self.servs += [[auth.rdata.lower(),None]]
                self.cycle(msg, None)
            else:
                if(self.done != None):
                    self.done(self.question, None)
                """
                auth = response.authority[
                    random.randint(0,
                                   response.header.nscount - 1)]
                for item in response.additional:
                    if(item.name.lower() == auth.rdata.lower() and
                       item.rtype == NWDNS.RTYPE_A):
                        msg = NWDNS.DNSMessage().cloneFrom(msg)
                        self.querier.send(msg,
                                          item.rdata,
                                          self.iterate)
                        return
                if(auth.rtype == NWDNS.RTYPE_SOA):
                    #raise NotImplementedError("Not handling SOA")
                    if(self.done != None):
                        self.done(self.question, None)
                else:
                    # Have a limit on recursive lookups 
                    # for NS RRSet: a chain of 10 at most
                    RecursiveLookup(self.querier, auth.rdata, 
                                    self.resume, NWDNS.RTYPE_A, self.limit + 1)
                """


# A cache indexed by (name, rrtype), which contains DNSMessages
# which have the answers in question
RRSetCache = {}

# A variant on RecursiveLookup that actually maintains a cache
# of names.  Note that it may return old/stale DNS messages 

# ToDo:  Add retry functionality
# ToDo:  Add limit on amount of recursion on the NS RRSet
class CachedLookup():
    def findNS(self, domain):
        lookup = (domain, NWDNS.RTYPE_NS)
        if lookup in RRSetCache:
            if(RRSetCache[lookup].header.ancount > 0 and
               time.time() - RRSetCache[lookup].timestamp 
               > RRSetCache[lookup].answer[0].ttl):
                del RRSetCache[lookup]
            if(RRSetCache[lookup].header.nscount > 0 and
               time.time() - RRSetCache[lookup].timestamp 
               > RRSetCache[lookup].authority[0].ttl):
                del RRSetCache[lookup]
            else:
                return RRSetCache[lookup]
        if(len(domain.split(".")) < 2):
            return None
        return self.findNS(domain.split(".",1)[1])

    def __init__(self, querier, name, done, qtype = NWDNS.RTYPE_A):
        self.querier = querier
        self.name = name
        self.done = done
        self.qtype = qtype
        self.question = (name, qtype)
        lookup = (name, qtype)
        if lookup in RRSetCache:
            if(time.time() - RRSetCache[lookup].timestamp 
               > RRSetCache[lookup].answer[0].ttl):
                del RRSetCache[lookup]
            elif(done != None):
                done(lookup, RRSetCache[lookup])
                return
        msg = NWDNS.DNSMessage()
        msg.question[0].qname = name
        msg.question[0].qtype = qtype
        server = RootServers[random.randint(0,len(RootServers) - 1)]
        ns = self.findNS(name)

        self.retryCount = 0

        if(ns != None):
            auth = ns.authority[
                random.randint(0,
                               ns.header.nscount - 1)]
            for item in ns.additional:
                if(item.name.lower() == auth.rdata.lower() and
                   item.rtype == NWDNS.RTYPE_A):
                    self.retryCount = 0
                    msg = NWDNS.DNSMessage().cloneFrom(msg)
                    self.querier.send(msg,
                                      item.rdata,
                                      self.iterate)
                    return 
            CachedLookup(self.querier, auth.rdata, self.resume)
            return

        msg = NWDNS.DNSMessage().cloneFrom(msg)
        self.querier.send(msg, 
                          RootServers[random.randint(0,len(RootServers) - 1)],
                          self.iterate)


    def resume(self, question, resume):
        msg = NWDNS.DNSMessage()
        msg.question[0].qname = self.name
        msg.question[0].qtype = self.qtype
        if(resume == None):
            if(self.done != None):
                self.done(self.question, None)
                return
        for item in resume.answer:
            if(item.rtype == NWDNS.RTYPE_A):
                msg = NWDNS.DNSMessage().cloneFrom(msg)
                self.querier.send(msg,
                                  item.rdata,
                                  self.iterate)
                return
        
        if(self.done != None):
            raise ValueError, "Don't know how to handle"
        # self.done(None)

    def iterate(self, msg, response):
        if(response == None):
            self.done(self.question, None)
        elif(response.header.rcode != NWDNS.RCODE_OK):
            self.done(self.question, response)
        else:
            if(response.header.ancount > 0):
                RRSetCache[(msg.question[0].qname, msg.question[0].qtype)] = response
                if(self.done != None):
                    self.done(self.question, response)
            else:
                if(response.header.nscount > 0):
                    RRSetCache[(response.authority[0].name, 
                                NWDNS.RTYPE_NS)] = response
                auth = response.authority[
                    random.randint(0,
                                   response.header.nscount - 1)]
                for item in response.additional:
                    if(item.name.lower() == auth.rdata.lower() and
                       item.rtype == NWDNS.RTYPE_A):
                        msg = NWDNS.DNSMessage().cloneFrom(msg)
                        self.querier.send(msg,
                                          item.rdata,
                                          self.iterate)
                        return
                if(auth.rtype == NWDNS.RTYPE_SOA):
                    raise NotImplementedError("Not handling SOA")
                else:
                    CachedLookup(self.querier, auth.rdata, self.resume)




# A class that does a lookup with recursion desired enabled to a
# specific server.

class DelegatedLookup():
    def __init__(self, querier, server, name, done, qtype = NWDNS.RTYPE_A,
                 do = False, edns_mtu = 0):
        self.querier = querier
        self.name = name
        self.done = done
        self.qtype = qtype
        self.question = (name, qtype)
        msg = NWDNS.DNSMessage()
        msg.question[0].qname = name
        msg.question[0].qtype = qtype
        msg.header.rd = True
        if do == True:
            if edns_mtu == 0:
                edns_mtu = 4096
            msg.additional = [NWDNS.DNSAnswer('',
                                                NWDNS.RTYPE_OPT)]
            msg.additional[0].rtype = NWDNS.RTYPE_OPT
            msg.additional[0].rclass = 4096
            msg.additional[0].ttl = 32768 #EDNS version 0, DO set
            msg.additional[0].rdlength = 0
            msg.additional[0].rdata = None
        elif edns_mtu != 0:
            msg.additional = [NWDNS.DNSAnswer('',
                                                NWDNS.RTYPE_OPT)]
            msg.additional[0].rtype = NWDNS.RTYPE_OPT
            msg.additional[0].rclass = 4096
            msg.additional[0].ttl = 0
            msg.additional[0].rdlength = 0
            msg.additional[0].rdata = None
        self.querier.send(msg, 
                          server,
                          self.response)
        self.retryCount = 0

    def response(self, msg, response):
        if(self.done != None):
            self.done(self.question, response)


# A class that does a direct lookup to a single server for probing it.
# The function done() is called with a tuple of (qname, qtype) as the
# first argument and thde final DNS answer as the second argument, or
# None if there is a failure (either a timeout or a lack of sufficient
# glue information)

class DirectLookup():
    def __init__(self, querier, server, name, done, qtype = NWDNS.RTYPE_A, 
                 do = False, edns_mtu = 0):
        self.querier = querier
        self.name = name
        self.done = done
        self.qtype = qtype
        self.question = (name, server, qtype)
        msg = NWDNS.DNSMessage()
        msg.question[0].qname = name
        msg.question[0].qtype = qtype

        if do == True:
            if edns_mtu == 0:
                edns_mtu = 4096
            msg.additional = [NWDNS.DNSAnswer('',
                                                NWDNS.RTYPE_OPT)]
            msg.additional[0].rtype = NWDNS.RTYPE_OPT
            msg.additional[0].rclass = 4096
            msg.additional[0].ttl = 32768 #EDNS version 0, DO set
            msg.additional[0].rdlength = 0
            msg.additional[0].rdata = None
        elif edns_mtu != 0:
            msg.additional = [NWDNS.DNSAnswer('',
                                                NWDNS.RTYPE_OPT)]
            msg.additional[0].rtype = NWDNS.RTYPE_OPT
            msg.additional[0].rclass = 4096
            msg.additional[0].ttl = 0
            msg.additional[0].rdlength = 0
            msg.additional[0].rdata = None

        self.querier.send(msg, 
                          server,
                          self.response)
        self.retryCount = 0

    def response(self, msg, response):
        if(self.done != None):
            self.done(self.question, response)


# A class that does a recursive lookup with no internal cache, to
# capture the NS RRSet associated with the final name and query.

# It does this by memoizing the previous response (from the one-above
# the final nameserver)


# Done gets 4 arguments rather than 2

# The first is the question tuple

# The second is the parent server's NS RRset

# The third is the final server's NS RRset

# and the fourth is the final answer

# For most servers, it is the parent's server's NS RRSet that should
# be probed repeatedly to get the final answer.  If there are
# A-records in the values, those should be used.  If not, a recursive
# lookup should be used to find the A records of the NS RRSet

# Basically, an attacker is pretty constrained here.  The parent
# server is almost certainly NOT under the attacker's control (thats
# the domain registrar), and MUST have A records in the additional
# field if they point internal to the final domain, and LIKELY have A
# records if they are internal to the parent's domain (eg, .com names
# from the .com server).

# If however, there are no A records in the additional field, those
# COULD be fast-fluxed as well.

class NSRRSetLookup():
    def __init__(self, querier, name, done, qtype = NWDNS.RTYPE_A):
        self.querier = querier
        self.name = name
        self.done = done
        self.qtype = qtype
        self.question = (name, qtype)
        self.last = None
        self.servs = []

        msg = NWDNS.DNSMessage()
        msg.question[0].qname = name
        msg.question[0].qtype = qtype
        """
        self.querier.send(msg, 
                          RootServers[random.randint(0,len(RootServers) - 1)],
                          self.iterate)
        """
        self.retryCount = 0

        self.servs.extend([[i, i] for i in RootServers])
        self.cycle(msg, None)


    class rrsetData():
        def __init__(self, domain, ttl, name, values):
            self.domain = domain
            self.ttl = ttl
            self.name = name
            self.values = values
        def __repr__(self):
            return "NS RRSET: Domain: %s Nameserver: %s TTL: %i Values: %s" \
                % (self.domain, self.name, self.ttl, self.values)

    def makeRRSet(self, message):
        if(message == None):
            return None
        rrset = []
        for item in message.authority:
            value = []
            for item2 in message.additional:
                if(item2.name == item.rdata):
                    value.append(item2)
            rrset.append(self.rrsetData(item.name, item.ttl,
                                   item.rdata, value))
                                   
        return rrset

    def resume(self, question, resume):
        msg = NWDNS.DNSMessage()
        msg.question[0].qname = self.name
        msg.question[0].qtype = self.qtype
        """
        if(resume == None):
            self.done(self.question, 
                      self.makeRRSet(self.last),
                      None, 
                      None)
            return
        """

        if (resume != None):
            servs = filter(lambda a: 
                a.rtype == NWDNS.RTYPE_A,
                resume.answer)
        
            if (servs != []):
                self.servs = map(lambda a: [a.name.lower(), a.rdata], servs) + self.servs
                resume = None
            else:
                resume.header.rcode = NWDNS.RCODE_SERVFAIL
        self.cycle(msg, resume)

        """
        for item in resume.answer:
            if(item.rtype == NWDNS.RTYPE_A):
                msg = NWDNS.DNSMessage().cloneFrom(msg)
                self.querier.send(msg,
                                  item.rdata,
                                  self.iterate)
                return

        # Unable to analyze this because no A record found
        # for the NS
        resume.header.rcode = NWDNS.RCODE_SERVFAIL
        self.done(self.question, self.makeRRSet(self.last),
                  None, resume)
        """

    def cycle(self, msg, response):
        if (response == None or response.header.rcode != NWDNS.RCODE_OK):
            try:
                [ns, addr] = self.servs.pop(0)
                if (addr):
                    msg = NWDNS.DNSMessage().cloneFrom(msg)
                    self.querier.send(msg,
                                      addr,
                                      self.cycle)
                else:
                    RecursiveLookup(self.querier, ns, 
                                    self.resume, NWDNS.RTYPE_A)
            except IndexError:
                self.iterate(msg, response)

        else:
            self.servs = []
            self.iterate(msg, response)

    def iterate(self, msg, response):
        if(response == None):
            self.done(self.question, self.makeRRSet(self.last),
                      None, None)
        elif(response.header.rcode != NWDNS.RCODE_OK):
            self.done(self.question, self.makeRRSet(self.last),
                      self.makeRRSet(response), response)
        else:

            if(response.header.ancount > 0):
                self.done(self.question,
                          self.makeRRSet(self.last),
                          self.makeRRSet(response),
                          response)
            elif(response.header.nscount == 0):
                self.done(self.question,
                          self.makeRRSet(self.last),
                          None,
                          response)
            else:
                self.last = response

                for auth in response.authority:
                    if(auth.rtype == NWDNS.RTYPE_SOA):
                        #raise NotImplementedError("Not handling SOA")
                        response.header.rcode = NWDNS.RCODE_SERVFAIL
                        if(self.done != None):
                            self.done(self.question, None, None, response)
                        return
                    else:
                        servs = filter(lambda a: a.name.lower() == auth.rdata.lower() and
                                a.rtype == NWDNS.RTYPE_A,
                                response.additional)
                        if (servs != []):
                            self.servs += map(lambda a: [a.name.lower(), a.rdata], servs)
                        else:
                            self.servs += [[auth.rdata.lower(),None]]
                self.cycle(msg, None)            

                """
                index = random.randint(0,response.header.nscount - 1)
                auth = response.authority[index]
                for item in response.additional:
                    if(item.name.lower() == auth.rdata.lower() and
                       item.rtype == NWDNS.RTYPE_A):
                        msg = NWDNS.DNSMessage().cloneFrom(msg)
                        self.querier.send(msg,
                                          item.rdata,
                                          self.iterate)
                        return
                if(auth.rtype == NWDNS.RTYPE_SOA):
                    #raise NotImplementedError("Not handling SOA")
                    response.header.rcode = NWDNS.RCODE_SERVFAIL
                    if(self.done != None):
                        self.done(self.question, None, None, response)
                else:
                    RecursiveLookup(self.querier, auth.rdata, 
                                    self.resume, NWDNS.RTYPE_A)
                """
